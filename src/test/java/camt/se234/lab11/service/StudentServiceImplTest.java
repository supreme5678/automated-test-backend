package camt.se234.lab11.service;

import camt.se234.lab11.dao.StudentDao;
import camt.se234.lab11.dao.StudentDaoImpl;
import camt.se234.lab11.entity.Student;
import com.fasterxml.jackson.core.sym.NameN;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class StudentServiceImplTest {
    StudentDao studentDao;
    StudentServiceImpl studentService;

    @Before
    public void setup(){
        studentDao = mock(StudentDao.class);
        studentService = new StudentServiceImpl();
        studentService.setStudentDao(studentDao);
    }

    @Test
    public void testFindById(){
        StudentDaoImpl studentDao = new StudentDaoImpl();
        StudentServiceImpl studentService = new StudentServiceImpl();
        studentService.setStudentDao(studentDao);
        assertThat(studentService.findStudentById("123"),is(new Student("123","A","temp",2.33)));
        assertThat(studentService.findStudentById("789"),is(new Student("789","C","temp",3.49)));
        assertThat(studentService.findStudentById("222"),is(new Student("222","E","temp",3.00)));
        assertThat(studentService.findStudentById("456"),is(new Student("456","B","temp",4.00)));
    }
    @Test
    public void testGetAverageGpa(){
        StudentDaoImpl studentDao = new StudentDaoImpl();
        StudentServiceImpl studentService = new StudentServiceImpl();
        studentService.setStudentDao(studentDao);
        assertThat(studentService.getAverageGpa(),is(2.914));
    }
    @Test
    public void testGetAverageNewGpa(){
        StudentDaoImpl studentDao = new StudentDaoImpl();
        StudentServiceImpl studentService = new StudentServiceImpl();
        studentService.setStudentDao(studentDao);
        assertThat(studentService.getAverageNewGpa(),is(2.474));
    }
    @Test
    public void testWithMock(){
        List<Student> mockStudents= new ArrayList<>();
        mockStudents.add(new Student("123","A","temp",2.33));
        mockStudents.add(new Student("124","B","temp",2.33));
        mockStudents.add(new Student("223","C","temp",2.33));
        mockStudents.add(new Student("224","D","temp",2.33));
        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.findStudentById("123"),is(new Student("123","A","temp",2.33)));
    }
    @Test
    public void testWithMockAgain(){
        List<Student> mockStudents= new ArrayList<>();
        mockStudents.add(new Student("232","E","temp",3.33));
        mockStudents.add(new Student("234","F","temp",3.33));
        mockStudents.add(new Student("113","G","temp",3.33));
        mockStudents.add(new Student("115","H","temp",3.33));
        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.findStudentById("234"),is(new Student("234","F","temp",3.33)));
    }
    @Test
    public void testWithMockAvgGpa(){
        //StudentDao studentDao = mock(StudentDao.class);
        List<Student> mockStudents= new ArrayList<>();
        //StudentServiceImpl studentService = new StudentServiceImpl();
        //studentService.setStudentDao(studentDao);
        mockStudents.add(new Student("232","E","temp",2.33));
        mockStudents.add(new Student("234","F","temp",2.33));
        mockStudents.add(new Student("113","G","temp",2.33));
        mockStudents.add(new Student("115","H","temp",2.33));
        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.getAverageGpa(),is(2.33));
    }
    @Test
    public void testWithMockAvgGpaAgain(){
        //StudentDao studentDao = mock(StudentDao.class);
        List<Student> mockStudents= new ArrayList<>();
        //StudentServiceImpl studentService = new StudentServiceImpl();
        //studentService.setStudentDao(studentDao);
        mockStudents.add(new Student("111","E","temp",4.00));
        mockStudents.add(new Student("112","F","temp",3.00));
        mockStudents.add(new Student("113","G","temp",2.00));
        mockStudents.add(new Student("114","H","temp",1.00));
        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.getAverageGpa(),is(2.5));
    }
    @Test
    public void testWithMockAvgGpaAgain2(){
        //StudentDao studentDao = mock(StudentDao.class);
        List<Student> mockStudents= new ArrayList<>();
        //StudentServiceImpl studentService = new StudentServiceImpl();
        //studentService.setStudentDao(studentDao);
        mockStudents.add(new Student("222","E","temp",4.00));
        mockStudents.add(new Student("233","F","temp",1.00));
        mockStudents.add(new Student("234","G","temp",1.00));
        mockStudents.add(new Student("235","H","temp",4.00));
        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.getAverageGpa(),is(2.5));
    }
    @Test
    public void testFindByPartOfId(){
        //StudentDao studentDao = mock(StudentDao.class);
        List<Student> mockStudents= new ArrayList<>();
        //StudentServiceImpl studentService = new StudentServiceImpl();
        //studentService.setStudentDao(studentDao);
        mockStudents.add(new Student("123","A","temp",2.33));
        mockStudents.add(new Student("124","B","temp",2.33));
        mockStudents.add(new Student("223","C","temp",2.33));
        mockStudents.add(new Student("224","D","temp",2.33));
        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.findStudentByPartOfId("22"),hasItem(new Student("223","C","temp",2.33)));
        assertThat(studentService.findStudentByPartOfId("22")
                ,hasItems(new Student("223","C","temp",2.33)
                        ,new Student("224","D","temp",2.33)));
    }
    @Test
    public void testFindByPartOfIdAgain(){
        //StudentDao studentDao = mock(StudentDao.class);
        List<Student> mockStudents= new ArrayList<>();
        //StudentServiceImpl studentService = new StudentServiceImpl();
        //studentService.setStudentDao(studentDao);
        mockStudents.add(new Student("001","E","temp",2.33));
        mockStudents.add(new Student("002","F","temp",2.33));
        mockStudents.add(new Student("003","G","temp",2.33));
        mockStudents.add(new Student("004","H","temp",2.33));
        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.findStudentByPartOfId("00"),hasItem(new Student("001","E","temp",2.33)));
        assertThat(studentService.findStudentByPartOfId("00")
                ,hasItems(new Student("001","E","temp",2.33)
                        ,new Student("002","F","temp",2.33)
                        ,new Student("003","G","temp",2.33)
                        ,new Student("004","H","temp",2.33)));
    }
    @Test(expected = NoDataException.class)
    public void testNoDataException(){
        List<Student> mockStudents = new ArrayList<>();
        mockStudents.add(new Student("123","A","temp",2.33));
        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.findStudentById("55"),nullValue());
    }

    @Test(expected = ArraySizeIsZero.class)
    public void testArrayIsZeroException(){
        List<Student> mockStudents = new ArrayList<>();
        mockStudents.add(new Student("123","A","temp",2.33));
        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.findStudentByPartOfId("88"),nullValue());

    }
    @Test(expected = Arithmetic.class)
    public void testDataIsDividedByZeroException(){
        List<Student> mockStudents = new ArrayList<>();
        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.getAverageGpa(),nullValue());

    }

}
