package camt.se234.lab11.service;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import junitparams.naming.TestCaseName;
import org.junit.Test;
import org.junit.runner.RunWith;


import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
//import static org.hamcrest.number.IsCloseTo.closeTo;

@RunWith(JUnitParamsRunner.class)
public class GradeServiceImplTest {

    @Test
    public void testGetGrade() {
        GradeServiceImpl gradeService = new GradeServiceImpl();
        assertThat(gradeService.getGrade(100), is("A"));
        assertThat(gradeService.getGrade(80), is("A"));
        assertThat(gradeService.getGrade(78.9), is("B"));
        assertThat(gradeService.getGrade(75), is("B"));
        assertThat(gradeService.getGrade(74.4), is("C"));
        assertThat(gradeService.getGrade(60), is("C"));
        assertThat(gradeService.getGrade(59.4), is("D"));
        assertThat(gradeService.getGrade(33), is("D"));
        assertThat(gradeService.getGrade(32), is("F"));
        assertThat(gradeService.getGrade(0), is("F"));
    }
    public Object paramsForTestGetGradeParams() {
        return new Object[][]{
                {100, "A"},
                {77, "B"}

        };
    }



    @Test
    @Parameters(method = "paramsForTestGetGradeParams")
    @TestCaseName("Test getGrade Params [{index}] : input is {0}, expect \"{1}\"")
    public void testGetGradeParams(double score, String expectedGrade) {
        GradeServiceImpl gradeService = new GradeServiceImpl();
        assertThat(gradeService.getGrade(score),is(expectedGrade));
    }

   @Test
    public void testTotalGrade(){
        GradeServiceImpl gradeService = new GradeServiceImpl();
        assertThat(gradeService.getGrade(50, 50),is("A"));
        assertThat(gradeService.getGrade(50,30),is("A"));
        assertThat(gradeService.getGrade(40,38.9),is("B"));
        assertThat(gradeService.getGrade(40,35),is("B"));
        assertThat(gradeService.getGrade(40,34.5),is("C"));
        assertThat(gradeService.getGrade(30,30),is("C"));
        assertThat(gradeService.getGrade(20,39.4),is("D"));
        assertThat(gradeService.getGrade(18,15),is("D"));
        assertThat(gradeService.getGrade(13.3,18.7),is("F"));
        assertThat(gradeService.getGrade(0,0),is("F"));
    }
    public Object paramsForTestTotalGradeParams(){
        return new Object[][]{
                {50, 50, "A"},
                {45, 32, "B"}
        };
    }
    @Test
    @Parameters(method = "paramsForTestTotalGradeParams")
    @TestCaseName("Test totalGrade Params [{index}] : input is {0} and {1}, expect \"{2}\"")
    public void testTotalGradeParams(double midtermScore, double finalScore,String expectedGrade){
        GradeServiceImpl gradeService = new GradeServiceImpl();
        assertThat(gradeService.getGrade(midtermScore, finalScore),is(expectedGrade));
    }



}
