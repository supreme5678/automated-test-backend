package camt.se234.lab11.dao;

import camt.se234.lab11.entity.Student;

import java.util.ArrayList;
import java.util.List;

public class StudentDaoImpl implements StudentDao {
    List<Student> students;
    List<Student> newstudents;
    public StudentDaoImpl(){
        students = new ArrayList<>();
        students.add(new Student("123","A","temp",2.33));
        students.add(new Student("456","B","temp",4.00));
        students.add(new Student("789","C","temp",3.49));
        students.add(new Student("111","D","temp",1.75));
        students.add(new Student("222","E","temp",3.00));

        newstudents = new ArrayList<>();
        newstudents.add(new Student("373","F","temp",3.33));
        newstudents.add(new Student("424","G","temp",1.00));
        newstudents.add(new Student("143","H","temp",3.79));
        newstudents.add(new Student("257","I","temp",1.75));
        newstudents.add(new Student("500","J","temp",2.50));
    }

    @Override
    public List<Student> findAll() {
        return this.students;
    }

    @Override
    public List<Student> findAllNew() {
        return this.newstudents;
    }
}
